/*
   Upper level check.i file for the Yeti plugin.

   Make sure the companion yeti_*_test.i files are in your Yorick path.

   run "yorick -i check.i", you should get a self-explanatory output.
*/

// Set paths to find local yeti version
if (dir=find_in_path("yeti.i", path="./:core/:../core/", takefirst=1)) {
  dir=dirname(dir);
  plug_dir,_(dir, plug_dir());
  set_path,dir+":"+get_path();
 }

write,"-> Trying to load Yeti plugin...";
#include "yeti.i"

write,"-> Checking availability of basic Yeti functions...";
#include "yeti_test.i"
write,"-> Basic Yeti functions present";

write,"-> Checking Yeti hash tables functions...";
#include "yeti_hash_test.i"
write,"-> Yeti hash tables functions OK";

write,"-> Checking Yeti hierarchical data format (YHD) functions...";
#include "yeti_yhdf.i"
#include "yeti_yhdf_test.i"
yhd_test,"yeti_yhdf_test.tmp";
write,"-> YHD functions: no error, you may want to check detailed output";

write,"-> Checking Yeti sparse matrix operation functions...";
#include "yeti_sparse_test.i"
write,"-> Sparse matrix operations: no error, you may want to check detailed output";

write,"-> Looking for Yeti-FFTW test suite...";
if (dir=find_in_path("yeti_fftw_test.i", path="./:fftw/:../fftw/:"+get_path(), takefirst=1)) {
  dir=dirname(dir);
  plug_dir,_(dir, plug_dir());
  set_path,dir+":"+get_path();
  include, "yeti_fftw.i", 1;
  include, "yeti_fftw_test.i", 1;
  write,"-> Yeti-FFTW loaded successfully, running now...";
  fftw_time,[2,256,256], repeat=10;
  fftw_compare,[2,256,256];
  write,"-> Yeti-FFTW tests successful.";
 } else write, "-> Yeti-FFTW test suite not found.";


write,"____________________________________________________________________________";
write,"";
write,"All tests passed without triggering errors.";
write,"";
write,"You may want to check detailed output:";
write,"Ouput values in sparse matrix operations should be 0 or very small,";
write,"A few \"*** value(s) differ for member ...\" messages are OK when checking";
write,"YHD functions with various foreign encodings (they indicate over/underflows";
write,"in encodings with types shorter than native).";
write,"____________________________________________________________________________";
write,"";
quit;
